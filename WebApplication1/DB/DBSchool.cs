﻿using Microsoft.EntityFrameworkCore;
using WebApplication1.Entities;

namespace WebApplication1.DB
{
    public class DBSchool : DbContext
    {
        public DBSchool(DbContextOptions<DBSchool> options ) : base ( options ) { 
        
            
        }
        public DbSet<Student> StudentsSchool { get; set; }

    }
}

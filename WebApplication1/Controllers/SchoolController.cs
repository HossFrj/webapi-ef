﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.DB;
using WebApplication1.Entities;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SchoolController : ControllerBase
    {
        private readonly DBSchool _context;

        public SchoolController(DBSchool context)
        {
            _context = context;
        }

        //[HttpGet]
        //public async Task<ActionResult<List<Student>>> SchoolList()
        //{
        //    var Students = new List<Student>
        //    {
        //        new Student
        //        {
        //            ID = 1,
        //            FullName = "HosseinFaraj"

        //        }
        //    };

        //   return Ok(Students);

        //}

        [HttpGet]
        public async Task<ActionResult<List<Student>>> ListStudent()
        {
            var Students = await _context.StudentsSchool.ToListAsync();

            return Ok(Students);

        }

        [HttpGet("{id}")]
        public async Task<ActionResult<List<Student>>> FindStudentByID(int id)
        {
            var Students = await _context.StudentsSchool.FindAsync(id);
            if (Students == null)
            {
                return NotFound();

            }
            return Ok(Students);

        }

        [HttpPost]
        public async Task<ActionResult<List<Student>>> AddNewStduent(Student newSudent)
        {
            _context.StudentsSchool.Add(newSudent);
            await _context.SaveChangesAsync();



            return Ok(await _context.StudentsSchool.ToListAsync());
        }

        [HttpPut]
        public async Task<ActionResult<List<Student>>> UpdateStudent(Student editStudent)
        {
            var Students = await _context.StudentsSchool.FindAsync(editStudent.ID);
            if (Students == null)
            {
                return NotFound();

            }
            else
            {
                Students.FullName = editStudent.FullName;
                Students.ID = editStudent.ID;
                //_context.StudentsSchool.Update(Students);
                await _context.SaveChangesAsync();

            }
            return Ok(await _context.StudentsSchool.ToListAsync());

        }
        [HttpDelete("{id}")]
        public async Task<ActionResult<List<Student>>> DeleteStudent (int id)
        {
            var Students = await _context.StudentsSchool.FindAsync(id);
            if (Students == null)
            {
                return NotFound();

            }
            _context.StudentsSchool.Remove(Students);
            await _context.SaveChangesAsync();
            return Ok(Students);

        }
    }
}
